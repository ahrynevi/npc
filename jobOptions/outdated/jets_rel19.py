##############################
#   serguei.yanush@cern.ch   #
##############################

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from AthenaCommon.AppMgr import ServiceMgr
MessageSvc = ServiceMgr.MessageSvc
MessageSvc.OutputLevel = ERROR

AthenaEventLoopMgr = Service("AthenaEventLoopMgr")
AthenaEventLoopMgr.OutputLevel = ERROR

from AthenaServices.AthenaServicesConf import AtRndmGenSvc
ServiceMgr += AtRndmGenSvc()
AtRndmGenSvc = ServiceMgr.AtRndmGenSvc
AtRndmGenSvc.SaveToFile = False


# any string without blanks
if not 'LABEL' in dir():
    LABEL = 'xxx'

if not 'ENERGY' in dir():
    ENERGY = '7000'

# Rivet analyses
#ANALYSES = ['MYJETS']
ANALYSES = ['TRIJETS']

# Number of events (string)
if not 'EVENTS' in dir():
    EVENTS = '100'

# pythia6, pythia8, herwig
if not 'GEN' in dir():
    GEN = 'pythia8'

# see below. TUNE is exclusive for every generator
if not 'TUNE' in dir():
    TUNE = '4C'

# AH = Non-pert. effects on
# NS = Non-pert. effect off
if not 'TYPE' in dir():
    TYPE = 'AH'

# JX samples
# J = 0 .. 8
if not 'J' in dir():
    J = '1'

# number of outgoing partons (Alpgen only)
# P = 2 .. 6
if not 'P' in dir():
    P = '2'
    
if not 'iSEED1' in dir():
    iSEED1 = '1'
if not 'iSEED2' in dir():
    iSEED2 = '1'
if not 'iSEED3' in dir():
    iSEED3 = '1'
if not 'iSEED4' in dir():
    iSEED4 = '1'
                            
import os,sys,binascii,subprocess,shutil,time,glob

print "ENERGY = ", ENERGY
print "EVENTS = ", EVENTS
theApp.EvtMax = int(EVENTS)

#iSEED1=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
#iSEED2=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
#iSEED3=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
#iSEED4=int(binascii.hexlify(os.urandom(4)), 16) % 1000000

SEED1 = "%06d" % (iSEED1)
SEED2 = "%06d" % (iSEED2)
SEED3 = "%06d" % (iSEED3)
SEED4 = "%06d" % (iSEED4)

print "SEEDS = ", SEED1, SEED2, SEED3, SEED4

if GEN=='pythia6':

    ## Base configuration for PYTHIA 6
    AtRndmGenSvc.Seeds = ["PYTHIA 4"+SEED1+" 989"+SEED2,"PYTHIA_INIT "+SEED3+" 2"+SEED4]
    AtRndmGenSvc.EventReseeding = False
    from Pythia_i.Pythia_iConf import Pythia
    topAlg += Pythia()
    
    if TUNE=='P2011':
        Pythia.Direct_call_to_pytune=350
    
    topAlg.Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # Weak mixing angle
    "pydat1 paru 102 0.23113",
    # Masses
    "pydat2 pmas 6 1 172.5",     # Top mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # Widths (these have no effect since widths are calculated perturbatively in PYTHIA)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

    ## Tune configuration
    if   TUNE=='AUET2LO': # AUET2 LO
        topAlg.Pythia.Tune_Name="ATLAS_20110001" 
    elif TUNE=='AUET2BLO': # AUET2B MRST LO** 
        topAlg.Pythia.Tune_Name="ATLAS_20110002"
    elif TUNE=='AUET2BCTEQ': # AUET2B CTEQ
        topAlg.Pythia.Tune_Name="ATLAS_20110003"
    elif TUNE=='AMBT2LO': # AMBT2 LO
        topAlg.Pythia.Tune_Name="ATLAS_20110101"
    elif TUNE=='AMBT2BLO': # AMBT2B LO
        topAlg.Pythia.Tune_Name="ATLAS_20110102"
    elif TUNE=='AMBT2BCTEQ': # AMBT2B CTEQ
        topAlg.Pythia.Tune_Name="ATLAS_20110103"
    elif TUNE=='AUET2BMSTW': # AUET2B MSTW
        topAlg.Pythia.Tune_Name="ATLAS_20110004"
    elif TUNE=='AUET2BCTEQ66': # AUET2B CTEQ66
        topAlg.Pythia.Tune_Name="ATLAS_20110005"
    elif TUNE=='AUET2BCT10': # AUET2B CT10
        topAlg.Pythia.Tune_Name="ATLAS_20110006"
    elif TUNE=='P0':
        topAlg.Pythia.Tune_Name="PYTUNE_320"
    elif TUNE=='PHARD':
        topAlg.Pythia.Tune_Name="PYTUNE_321"
    elif TUNE=='PSOFT':
        topAlg.Pythia.Tune_Name="PYTUNE_322"
    elif TUNE=='P6':
        topAlg.Pythia.Tune_Name="PYTUNE_326"
    elif TUNE=='P2011': # Perugia2011 (CTEQ5L PDF)
        pass
        #topAlg.Pythia.Tune_Name="PYTUNE_350"
    elif TUNE=='P2011NOCR':
        topAlg.Pythia.Tune_Name="PYTUNE_354"
    elif TUNE=='P2011M':
        topAlg.Pythia.Tune_Name="PYTUNE_355"
    elif TUNE=='P2011C': # Perugia2011C (CTEQ6L1 PDF)
        topAlg.Pythia.Tune_Name="PYTUNE_356"
    elif TUNE=='P2011CMOD': # Perugia2011C (CTEQ6L1 PDF), modified
        topAlg.Pythia.Tune_Name="PYTUNE_356"
        topAlg.Pythia.PygiveCommand += [ "mstp(72)=1" ]
    elif TUNE=='P2012': # Perugia2012 (CTEQ6L1 PDF) from v6.427
        topAlg.Pythia.Tune_Name="PYTUNE_370"
    else:
        print 'Error: Wrong tune ID!'
        exit()

    # Type configuration
    if TYPE=='AH': # NP effects ON (default)
        pass
    elif TYPE=='NH': # just for checks
        topAlg.Pythia.PygiveCommand += [ "mstp(81)=20" ] # UE off
    elif TYPE=='AS': # just for checks
        topAlg.Pythia.PygiveCommand += [ "mstp(111)=0" ] # HAD off
    elif TYPE=='NN': # just for checks
        topAlg.Pythia.PygiveCommand += [ "mstp(81)=20" ] # UE off
        topAlg.Pythia.PygiveCommand += [ "mstp(111)=0" ] # HAD off
    elif TYPE=='NS': # NP effects OFF
        topAlg.Pythia.PygiveCommand += [ "mstp(81)=20" ] # UE off
        topAlg.Pythia.PygiveCommand += [ "mstp(111)=0" ] # HAD off
        topAlg.Pythia.PygiveCommand += [ "mstp(91)=0" ] # Turn off primordial kT
    else:
        print 'Error: Wrong type ID!'
        exit()

    # J sample configuration
    topAlg.Pythia.PythiaCommand += ["pysubs msel 0"]
    if J=='0':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 8."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 17."]
    elif J=='1':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 17."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 35."]
    elif J=='2':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 35."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 70."]
    elif J=='3':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 70."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 140."]
    elif J=='4':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 140."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 280."]
    elif J=='5':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 280."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 560."]
    elif J=='6':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 560."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 1120."]
    elif J=='7':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 1120."]
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 4 2240."]
    elif J=='8':
        topAlg.Pythia.PythiaCommand += ["pysubs ckin 3 2240."]
    else:
        print 'Error: Wrong J sample ID!'
        exit()

    # final tweaks
    topAlg.Pythia.PythiaCommand += ["pysubs msub 11 1","pysubs msub 12 1","pysubs msub 13 1","pysubs msub 28 1","pysubs msub 53 1","pysubs msub 68 1"]
    topAlg.Pythia.PythiaCommand += ["pyinit use_PYINIT CMS P P "+ENERGY+".0"]

if GEN=='pythia8':

    ## Base config for Pythia8
    AtRndmGenSvc.Seeds = ["PYTHIA8 4"+SEED1+" 989"+SEED2,"PYTHIA8_INIT "+SEED3+" 2"+SEED4]
    AtRndmGenSvc.EventReseeding = False
    from Pythia8_i.Pythia8_iConf import Pythia8_i
    topAlg += Pythia8_i("Pythia8")
    
    topAlg.Pythia8.Commands += [
    "Main:timesAllowErrors = 500",
    "6:m0 = 172.5",
    "23:m0 = 91.1876",
    "23:mWidth = 2.4952",
    "24:m0 = 80.399",
    "24:mWidth = 2.085",
    "StandardModel:sin2thetaW = 0.23113",
    "StandardModel:sin2thetaWbar = 0.23146",
    "ParticleDecays:limitTau0 = on",
    "ParticleDecays:tau0Max = 10.0"]
    
    if TUNE=='4C':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = cteq6ll.LHpdf"]
    elif TUNE=='AU2CTEQ':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = cteq6ll.LHpdf",
       "MultipartonInteractions:bProfile = 4",
       "MultipartonInteractions:a1 = 0.00",
       "MultipartonInteractions:pT0Ref = 2.13",
       "MultipartonInteractions:ecmPow = 0.21",
       "BeamRemnants:reconnectRange = 2.21",
       "SpaceShower:rapidityOrder=0"]
    elif TUNE=='AU2CT10':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = CT10.LHgrid",
       "MultipartonInteractions:bProfile = 4",
       "MultipartonInteractions:a1 = 0.10",
       "MultipartonInteractions:pT0Ref = 1.70",
       "MultipartonInteractions:ecmPow = 0.16",
       "BeamRemnants:reconnectRange = 4.67",
       "SpaceShower:rapidityOrder=0"]
    elif TUNE=='AU2MSTW':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
       "MultipartonInteractions:bProfile = 4",
       "MultipartonInteractions:a1 = 0.01",
       "MultipartonInteractions:pT0Ref = 1.87",
       "MultipartonInteractions:ecmPow = 0.28",
       "BeamRemnants:reconnectRange = 5.32",
       "SpaceShower:rapidityOrder=0"]
    elif TUNE=='AU2NNPDF':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = NNPDF21_100.LHgrid",
       "MultipartonInteractions:bProfile = 4",
       "MultipartonInteractions:a1 = 0.08",
       "MultipartonInteractions:pT0Ref = 1.74",
       "MultipartonInteractions:ecmPow = 0.17",
       "BeamRemnants:reconnectRange = 8.36",
       "SpaceShower:rapidityOrder=0"]
    elif TUNE=='A2CTEQ':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = cteq6ll.LHpdf",
       "MultipartonInteractions:bProfile = 4",
       "MultipartonInteractions:a1 = 0.06",
       "MultipartonInteractions:pT0Ref = 2.18",
       "MultipartonInteractions:ecmPow = 0.22",
       "BeamRemnants:reconnectRange = 1.55",
       "SpaceShower:rapidityOrder=0"]
    elif TUNE=='A2MSTW':
       topAlg.Pythia8.Commands += [
       "Tune:pp = 5",
       "PDF:useLHAPDF = on",
       "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
       "MultipartonInteractions:bProfile = 4",
       "MultipartonInteractions:a1 = 0.03",
       "MultipartonInteractions:pT0Ref = 1.90",
       "MultipartonInteractions:ecmPow = 0.30",
       "BeamRemnants:reconnectRange = 2.28",
       "SpaceShower:rapidityOrder=0"]
    else:
        print 'Error: Wrong tune ID!'
        exit()

    topAlg.Pythia8.Commands += ["HardQCD:all = on"]
    
    if TYPE=='AH':
        pass
        #Pythia8.Commands += [
        #"PartonLevel:MPI = on",
        #"HadronLevel:all = on"
        #]
    elif TYPE=='NH':
        topAlg.Pythia8.Commands += [
        "PartonLevel:MPI = off",
        #"HadronLevel:all = on"
        ]
    elif TYPE=='AS':
        topAlg.Pythia8.Commands += [
        #"PartonLevel:MPI = on",
        "HadronLevel:all = off",
        "Check:event = off"
        ]
    elif TYPE=='NN':
        topAlg.Pythia8.Commands += [
        "PartonLevel:MPI = off",
        "HadronLevel:all = off",
        "Check:event = off"
        ]
    elif TYPE=='NS':
        topAlg.Pythia8.Commands += [
        "PartonLevel:MPI = off",
        "HadronLevel:all = off",
        "BeamRemnants:primordialKT = off",
        "Check:event = off"
        ]
    else:
        print 'Error: Wrong type ID!'
        exit()
        
  
    if J=='0':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 8."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 17."]
    elif J=='1':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 17."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 35."]
    elif J=='2':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 35."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 70."]
    elif J=='3':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 70."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 140."]
    elif J=='4':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 140."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 280."]
    elif J=='5':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 280."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 560."]
    elif J=='6':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 560."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 1120."]
    elif J=='7':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 1120."]
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMax = 2240."]
    elif J=='8':
        topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 2240."]
    else:
        print 'Error: Wrong J sample ID!'
        exit()
        
    topAlg.Pythia8.CollisionEnergy = float(ENERGY)


if GEN=='herwigpp':

   ServiceMgr.AtRndmGenSvc.Seeds = ["Herwigpp 4"+SEED1+" 989"+SEED2]
   ServiceMgr.AtRndmGenSvc.EventReseeding = True
   from Herwigpp_i.Herwigpp_iConf import Herwigpp
   topAlg += Herwigpp()
   from Herwigpp_i import config as hw

   cmds = """
set /Herwig/Model:EW/Sin2ThetaW 0.23113
"""   
   ## Tunes
   if TUNE=='LOEE3':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
           + hw.lo_pdf_cmds("MRSTMCal.LHgrid") \
           + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % int(ENERGY))
   elif TUNE=='LOEE3CT10':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
           + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
           + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % int(ENERGY))
   elif TUNE=='CTEQEE3':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
          + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % int(ENERGY))
   elif TUNE=='CTEQEE3CT10':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
          + hw.nlo_pdf_cmds("CT10.LHgrid", "cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % int(ENERGY))
   elif TUNE=='LOEE4':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
          + hw.lo_pdf_cmds("MRSTMCal.LHgrid") \
          + hw.ue_tune_cmds("UE-EE-4-LO**")
   elif TUNE=='LOEE4CT10':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
           + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
           + hw.ue_tune_cmds("UE-EE-4-LO**")
   elif TUNE=='CTEQEE4':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
          + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("UE-EE-4-CTEQ6L1")
   elif TUNE=='CTEQEE4CT10':
      cmds += hw.energy_cmds(int(ENERGY)) + hw.base_cmds() \
          + hw.nlo_pdf_cmds("CT10.LHgrid", "cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("UE-EE-4-CTEQ6L1")
   else:
      print 'Error: Wrong tune ID!'
      exit()

   # We'll turn UE/HAD off as indicated in tune
   if TYPE=='AH':
      pass
   elif TYPE=='AS':
      cmds += """
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
   elif TYPE=='NH':
      cmds += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
"""
   elif TYPE=='NS':
      cmds += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
   else:
      print 'Error: Wrong type ID!'
      exit()

   # J samples
   if J=='0':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 17*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
set /Herwig/Cuts/QCDCuts:X1Min 0.01
set /Herwig/Cuts/QCDCuts:X2Min 0.01
"""
   elif J=='1':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 17*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 35*GeV
"""
   elif J=='2':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 35*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 70*GeV
"""
   elif J=='3':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 70*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 140*GeV
"""
   elif J=='4':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 140*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 280*GeV
"""
   elif J=='5':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 280*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 560*GeV
"""
   elif J=='6':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 560*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 1120*GeV
"""
   elif J=='7':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 1120*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 2240*GeV
"""
   elif J=='8':
      cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 2240*GeV
"""
   else:
      print 'Error: Wrong J sample ID!'
      exit()
   
   # last tweaks
   cmds += """
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Generators/LHCGenerator:MaxErrors 100000000
"""

   topAlg.Herwigpp.Commands += cmds.splitlines()
   print
   print cmds
   print
   #print cmds.splitlines()
   

if GEN=='alpgen':

    # good feature
    from AthenaCommon import Logging
    log = Logging.logging.getLogger('MyGeneration')

    events_athena=int(EVENTS)

    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')

    ## Base configuration for PYTHIA 6
    from Pythia_i.Pythia_iConf import Pythia
    AtRndmGenSvc.Seeds = ["PYTHIA 4"+SEED1+" 989"+SEED2,"PYTHIA_INIT "+SEED3+" 2"+SEED4]
    AtRndmGenSvc.EventReseeding = False
    topAlg += Pythia()
    Pythia = topAlg.Pythia
    Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # Weak mixing angle
    "pydat1 paru 102 0.23113",
    # Masses
    "pydat2 pmas 6 1 172.5",     # Top mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # Widths (these have no effect since widths are calculated perturbatively in PYTHIA)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]


    if TUNE=='AUET2LO':
        Pythia.Tune_Name="ATLAS_20110001"
    elif TUNE=='AUET2BLO':
        Pythia.Tune_Name="ATLAS_20110002"
    elif TUNE=='AUET2BCTEQ':
        Pythia.Tune_Name="ATLAS_20110003"
    elif TUNE=='AMBT2LO':
        Pythia.Tune_Name="ATLAS_20110101"
    elif TUNE=='AMBT2BLO':
        Pythia.Tune_Name="ATLAS_20110102"
    elif TUNE=='AMBT2BCTEQ':
        Pythia.Tune_Name="ATLAS_20110103"
    elif TUNE=='P2011':
        # PYTUNE 350: Perugia2011 (CTEQ5L PDF)
        Pythia.Tune_Name="ATLAS_-1"
        Pythia.Direct_call_to_pytune=350
    elif TUNE=='P2011C':
        # PYTUNE 356: Perugia2011C (CTEQ6L1 PDF)
        Pythia.Tune_Name="ATLAS_-1"
        Pythia.Direct_call_to_pytune=356
    elif TUNE=='P2012':
        # PYTUNE 370: Perugia2012 (CTEQ6L1 PDF) from v6.427
        Pythia.Tune_Name="ATLAS_-1"
        Pythia.Direct_call_to_pytune=370
    else:
        print 'Error: Wrong tune ID!'
        exit()

    Pythia.PythiaCommand += ["pyinit user alpgen","pydat1 parj 90 20000.","pydat3 mdcy 15 1 0","pypars mstp 143 1"]

    if TYPE=='AH':
        pass # default
    elif TYPE=='AS':
        Pythia.PygiveCommand += [ "mstp(111)=0" ] # HAD off
    elif TYPE=='NH':
        Pythia.PygiveCommand += [ "mstp(81)=20" ] # UE off
    elif TYPE=='NS':
        Pythia.PygiveCommand += [ "mstp(81)=20" ] # UE off
        Pythia.PygiveCommand += [ "mstp(111)=0" ] # HAD off
        Pythia.PygiveCommand += [ "mstp(91)=0" ] # Turn off primordial kT
    else:
        print 'Error: Wrong type ID!'
        exit()

    ## Tauola config
    from Tauola_i.Tauola_iConf import Tauola
    topAlg += Tauola()
    topAlg.Tauola.TauolaCommand = ["tauola polar 1","tauola radcor 1","tauola phox 0.01","tauola dmode 0","tauola jak1 0","tauola jak2 0"]

    ## Photos config
    from Photos_i.Photos_iConf import Photos
    topAlg += Photos()
    topAlg.Photos.PhotosCommand = ["photos pmode 1","photos xphcut 0.01","photos alpha -1.","photos interf 1","photos isec 1","photos itre 0","photos iexp 1","photos iftop 0"]

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
!...Matching parameters...
IEXCFILE=%i
showerkt=T
qcut=%i
imss(21)=24
imss(22)=24  
"""%(0,15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)
    ### 0 or 1 ??? RECHECK!!!
    phojf.write(phojinp)
    phojf.close()






from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
rivet.AnalysisPath = os.getenv("PWD")
#rivet.CrossSection = 1.0 # in nb
rivet.OutputLevel = INFO
rivet.Analyses = ANALYSES
topAlg += rivet

for ana in ANALYSES:
   NAME=LABEL+"_"+ana+"_"+ENERGY+"GeV_"+EVENTS+"_"+GEN+"_"+TUNE+"_"+TYPE+"_J"+J
   if GEN == "alpgen":
       NAME=LABEL+"_"+ana+"_"+ENERGY+"GeV_"+EVENTS+"_"+GEN+"_"+TUNE+"_"+TYPE+"_P"+P+"_J"+J
   NAME += "_"+SEED1+SEED2+".root"
   os.putenv(ana+"_OUTPUTFILE", NAME)
