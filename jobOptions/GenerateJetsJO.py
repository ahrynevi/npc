## Sherpa config with CT10 PDF
## include("MC15JobOptions/Sherpa_Base_Fragment.py")
## Base config for Sherpa
import os,binascii

# iSEED1=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
# iSEED2=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
# iSEED3=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
# iSEED4=int(binascii.hexlify(os.urandom(4)), 16) % 1000000

# SEED1 = "%06d" % (iSEED1)
# SEED2 = "%06d" % (iSEED2)
# SEED3 = "%06d" % (iSEED3)
# SEED4 = "%06d" % (iSEED4)

#print "SEEDS = ", SEED1, SEED2, SEED3, SEED4

GEN=""
try:
   GEN=str(os.environ['NPC_GEN'])
except KeyError:
   print "WARNING: using default generator"
   GEN="Pythia8"
print 'GEN =', GEN
#exit()

TUNE=""
try:
   TUNE=str(os.environ['NPC_TUNE'])
except KeyError:
   print "WARNING: using default tune"
   TUNE="4C"
   print "ERROR: Wrong tune set in environment"
   #exit()

print 'TUNE =', TUNE


JSAMPLE=0
try:
   JSAMPLE=int(os.environ['NPC_JSAMPLE'])
except KeyError:
   print "WARNING: using default JSAMPLE"
   JSAMPLE=-1

print 'JSAMPLE =',JSAMPLE

ecmEnergy=0
try:
   ecmEnergy=runArgs.ecmEnergy
except KeyError:
   print "WARNING: using default ecmEnergy"
   ecmEnergy=14000

print 'ecmEnergy =',ecmEnergy

jetPtMin=25
jetPtMax=14000

if JSAMPLE==-1:
   jetPtMin=25
   jetPtMax=14000
if JSAMPLE==1:
   jetPtMin=17
   jetPtMax=25
elif JSAMPLE==2:
   jetPtMin=25
   jetPtMax=70
elif JSAMPLE==3:
   jetPtMin=70
   jetPtMax=140
elif JSAMPLE==4:
   jetPtMin=140
   jetPtMax=280
elif JSAMPLE==5:
   jetPtMin=280
   jetPtMax=560
elif JSAMPLE==6:
   jetPtMin=560
   jetPtMax=1120
elif JSAMPLE==7:
   jetPtMin=1120
   jetPtMax=2240
elif JSAMPLE==8:
   jetPtMin=2240
   jetPtMax=3000
elif JSAMPLE==9:
   jetPtMin=3000
   jetPtMax=5500

print "jetPtMin=%d, jetPtMax=%d" % (jetPtMin,jetPtMax)

#############################
#### Pythia8 defs
if runArgs.runNumber==100000: #normal 
   TYPE = 'NS'

elif runArgs.runNumber==100001: #UE+Had
   TYPE = 'AH'

elif runArgs.runNumber==100002: #UE
   TYPE = 'AS'

elif runArgs.runNumber==100003: #Hadr
   TYPE = 'NH'

else:
   print 'ERROR: Wrong run number!'
   exit()


print "Working with ", GEN, TYPE


if GEN=='Pythia8':
   print 'Running on Pythia8'

   evgenConfig.generators += ["Pythia8"]
   evgenConfig.description = "Pythia8 QCD jets"
   evgenConfig.keywords = ["QCD","jets"]
   evgenConfig.contact  = ["Aliaksei Hrynevich"]

   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   
   ## Base config for Pythia8
   #AtRndmGenSvc.Seeds = ["PYTHIA8 4"+SEED1+" 989"+SEED2,"PYTHIA8_INIT "+SEED3+" 2"+SEED4]
   #AtRndmGenSvc.EventReseeding = False
   from Pythia8_i.Pythia8_iConf import Pythia8_i
   genSeq += Pythia8_i("Pythia8")
   
   genSeq.Pythia8.Commands += [
      "PDF:useLHAPDF=on",
      "PDF:LHAPDFset=NNPDF30_nlo_as_0118",
      "Main:timesAllowErrors = 500",
      "6:m0 = 172.5",
      "23:m0 = 91.1876",
      "23:mWidth = 2.4952",
      "24:m0 = 80.399",
      "24:mWidth = 2.085",
      "StandardModel:sin2thetaW = 0.23113",
      "StandardModel:sin2thetaWbar = 0.23146",
      "ParticleDecays:limitTau0 = on",
      "ParticleDecays:tau0Max = 10.0"
      ]
   
   if TUNE=='4C':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = cteq6ll.LHpdf"]
   elif TUNE=='AU2CTEQ':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = cteq6ll.LHpdf",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.00",
         "MultipartonInteractions:pT0Ref = 2.13",
         "MultipartonInteractions:ecmPow = 0.21",
         "BeamRemnants:reconnectRange = 2.21",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2CT10':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = CT10.LHgrid",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.10",
         "MultipartonInteractions:pT0Ref = 1.70",
         "MultipartonInteractions:ecmPow = 0.16",
         "BeamRemnants:reconnectRange = 4.67",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2MSTW':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.01",
         "MultipartonInteractions:pT0Ref = 1.87",
         "MultipartonInteractions:ecmPow = 0.28",
         "BeamRemnants:reconnectRange = 5.32",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2NNPDF':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF21_100.LHgrid",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.08",
         "MultipartonInteractions:pT0Ref = 1.74",
         "MultipartonInteractions:ecmPow = 0.17",
         "BeamRemnants:reconnectRange = 8.36",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='A2CTEQ':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = cteq6ll.LHpdf",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.06",
         "MultipartonInteractions:pT0Ref = 2.18",
         "MultipartonInteractions:ecmPow = 0.22",
         "BeamRemnants:reconnectRange = 1.55",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='A2MSTW':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.03",
         "MultipartonInteractions:pT0Ref = 1.90",
         "MultipartonInteractions:ecmPow = 0.30",
         "BeamRemnants:reconnectRange = 2.28",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2MSTW2008LO':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.01",
         "MultipartonInteractions:pT0Ref = 1.87",
         "MultipartonInteractions:ecmPow = 0.28",
         "BeamRemnants:reconnectRange = 5.32",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='MONASH':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF=on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='CMSMONASHSTAR':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:ee = 7",
#	"Tune:pp = 18",
#	"PDF:useLHAPDF=on",
#	"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='CMSCUETP8S1CTEQ6L1':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        " Tune:ee = 7",
#	"Tune:pp = 15",
#	"PDF:useLHAPDF=on",
#	"PDF:LHAPDFset = cteq6ll.LHpdf"]
#    elif TUNE=='CMSCUETP8S1HERAPDF1.5LO':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        " Tune:ee = 7",
#	"Tune:pp = 15",
#	"PDF:useLHAPDF=on",
#	"PDF:LHAPDFset = HERAPDF1.5LO"]
   elif TUNE=='ATLASA14CTEQL1':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7", 
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = cteq6ll",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.144",
         "SpaceShower:pT0Ref = 1.30",
         "SpaceShower:pTmaxFudge = 0.95",
         "SpaceShower:pTdampFudge = 1.21",
         "SpaceShower:alphaSvalue = 0.125",
         "TimeShower:alphaSvalue = 0.126",
         "BeamRemnants:primordialKThard = 1.72",
         "MultipartonInteractions:pT0Ref = 1.98",
         "MultipartonInteractions:alphaSvalue = 0.118",
         "BeamRemnants:reconnectRange = 2.08"]	
   elif TUNE=='ATLASA14MRSTWLO':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7", 
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = MSTW2008lo68cl",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.62",
         "SpaceShower:pTmaxFudge = 0.92",
         "SpaceShower:pTdampFudge = 1.14",
         "SpaceShower:alphaSvalue = 0.129",
         "TimeShower:alphaSvalue = 0.129",
         "BeamRemnants:primordialKThard = 1.82",
         "MultipartonInteractions:pT0Ref = 2.22",
         "MultipartonInteractions:alphaSvalue = 0.127",
         "BeamRemnants:reconnectRange = 1.87"]
   elif TUNE=='ATLASA14NNPDF':
      # based on jobFragments from J Ferrando
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
        "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
        "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG1p':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
        "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.73",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.131",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG1n':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.69"
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.121",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG2p':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.60",
         "SpaceShower:pTmaxFudge = 1.05",
         "SpaceShower:pTdampFudge = 1.04",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.139",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG2n':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.50",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.08",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.111",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG3ap':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.67",
         "SpaceShower:pTmaxFudge = 0.98",
         "SpaceShower:pTdampFudge = 1.36",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.136",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.125",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG3an':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.51",
         "SpaceShower:pTmaxFudge = 0.88",
         "SpaceShower:pTdampFudge = 0.93",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.124",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.127",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG3bp':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 1.00",
         "SpaceShower:pTdampFudge = 1.04",
         "SpaceShower:alphaSvalue = 0.129",
         "TimeShower:alphaSvalue = 0.114",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG3bn':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.83",
         "SpaceShower:pTdampFudge = 1.07",
         "SpaceShower:alphaSvalue = 0.126",
         "TimeShower:alphaSvalue = 0.138",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG3cp':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.140",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
   elif TUNE=='ATLASA14NNPDFEIG3cn':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         "PDF:useLHAPDF = on",
         "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.115",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         "BeamRemnants:reconnectRange = 1.71"]
# the below only works for Pythia8 see
# http://home.thep.lu.se/~torbjorn/pythia82html/Welcome.html
#    elif TUNE=='ATLASA14CTEQL1':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 19",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = cteq6ll.LHpdf"]
#    elif TUNE=='ATLASA14MSTW':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 20",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = MSTW2008lo68cl"]
#    elif TUNE=='ATLASA14HERAPDF':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 22",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = HERAPDF1.5LO"]
#    elif TUNE=='ATLASA14NNPDF':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 21",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG1p':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 23",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG1n':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 24",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG2p':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 25",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG2n':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 26",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3ap':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 27",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3an':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 28",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3bp':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 29",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3bn':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 30",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3cp':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 31",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3cn':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 32",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
   else:
      print 'Error: Wrong tune ID!', TUNE
      exit()
       
   genSeq.Pythia8.Commands += ["HardQCD:all = on"]
       
   if TYPE=='AH':
      pass
        #Pythia8.Commands += [
        #"PartonLevel:MPI = on",
        #"HadronLevel:all = on"
        #]
   elif TYPE=='NH':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         #"HadronLevel:all = on"
         ]
   elif TYPE=='AS':
      genSeq.Pythia8.Commands += [
         #"PartonLevel:MPI = on",
         "HadronLevel:all = off",
         "Check:event = off"
         ]
   elif TYPE=='NN':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         "HadronLevel:all = off",
         "Check:event = off"
         ]
   elif TYPE=='NS':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         "HadronLevel:all = off",
         "BeamRemnants:primordialKT = off",
         "Check:event = off"
         ]
   else:
      print 'Error: Wrong type ID!'
      exit()
      
      
   genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = %d" % jetPtMin]
   genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMax = %d" % jetPtMax]
   
   genSeq.Pythia8.CollisionEnergy = float(runArgs.ecmEnergy)
   evgenConfig.minevents = runArgs.maxEvents

elif GEN=='Herwigpp':
   print 'Running on Herwigpp'

   evgenConfig.generators += ["Herwigpp"]
   evgenConfig.description = "Herwigpp QCD jets"
   evgenConfig.keywords = ["QCD","jets"]
   evgenConfig.contact  = ["Aliaksei Hrynevich"]
   #insert SubProcess:MatrixElements[0] MEQCD2to2
   #from Pythia8_i.Pythia8_iConf import Pythia8_i
   #genSeq += Pythia8_i("Pythia8")

   #ServiceMgr.AtRndmGenSvc.Seeds = ["Herwigpp 4"]
   #ServiceMgr.AtRndmGenSvc.EventReseeding = True
   from Herwigpp_i.Herwigpp_iConf import Herwigpp
   genSeq += Herwigpp()
   from Herwigpp_i import config as hw

   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   cmds = ""

   ## Tunes
   if TUNE=='LOEE3':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.lo_pdf_cmds("MRSTMCal.LHgrid") \
          + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % (ecmEnergy))
   elif TUNE=='LOEE3CT10':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
          + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % (ecmEnergy))
   elif TUNE=='CTEQEE3':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % (ecmEnergy))
   elif TUNE=='CTEQEE3CT10':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.nlo_pdf_cmds("CT10.LHgrid", "cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % (ecmEnergy))
   elif TUNE=='LOEE4':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.lo_pdf_cmds("MRSTMCal.LHgrid") \
          + hw.ue_tune_cmds("UE-EE-4-LO**")
   elif TUNE=='LOEE4CT10':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
          + hw.ue_tune_cmds("UE-EE-4-LO**")
   elif TUNE=='CTEQEE4':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("UE-EE-4-CTEQ6L1")
   elif TUNE=='CTEQEE4CT10':
      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
          + hw.nlo_pdf_cmds("CT10.LHgrid", "cteq6ll.LHpdf") \
          + hw.ue_tune_cmds("UE-EE-4-CTEQ6L1")
   elif TUNE=='UEEE5CTEQ6L1':
      cmds += hw.energy_cmds(int(ecmEnergy)) + hw.base_cmds() \
       + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
       + hw.ue_tune_cmds("UE-EE-5-CTEQ6L1")
   else:
      print 'Error: Wrong tune ID!'
      exit()
      
   ###We'll turn UE/HAD off as indicated in tune
   if TYPE=='AH':
      pass
   elif TYPE=='AS':
      cmds += """
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
   elif TYPE=='NH':
      cmds += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
"""
   elif TYPE=='NS':
      cmds += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
   else:
      print 'Error: Wrong type ID!'
      exit()
 
   cmds += """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
""" 

#These are set by default apparently
#set /Herwig/Particles/t:NominalMass 172.5*GeV
#set /Herwig/Particles/tbar:NominalMass 172.5*GeV
#set /Herwig/Particles/W+:NominalMass 80.399*GeV
#set /Herwig/Particles/W-:NominalMass 80.399*GeV
#set /Herwig/Particles/Z0:NominalMass 91.1876*GeV
#set /Herwig/Particles/W+:Width 2.085*GeV
#set /Herwig/Particles/W-:Width 2.085*GeV
#set /Herwig/Particles/Z0:Width 2.4952*GeV
#set /Herwig/Model:EW/Sin2ThetaW 0.23113                                                                                                          

     
  ### J samples  
   cmds += """
set /Herwig/Cuts/JetKtCut:MinKT %d*GeV                               
set /Herwig/Cuts/JetKtCut:MaxKT %d*GeV                                        
""" % (jetPtMin,jetPtMax)
         
  ###last tweaks
###   cmds += """
###set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
###set /Herwig/Generators/LHCGenerator:MaxErrors 1000000
###"""

   genSeq.Herwigpp.Commands += cmds.splitlines()
   print
   print cmds
   print
  #### print cmds.splitlines()


#ANALYSES = ['InclusiveJets']
ANALYSES = ['LeadingJets']

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
rivet.AnalysisPath = os.getenv("PWD")
rivet.OutputLevel = INFO
rivet.Analyses = ANALYSES
genSeq += rivet
