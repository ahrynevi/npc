sherpaRunCard="""                                                             
                                                                           
(beam){                                                                        
BEAM_1 2212                                                                   
BEAM_ENERGY_1 7000                                                            
BEAM_2 2212                                                                   
BEAM_ENERGY_2 7000                                                            
}(beam)                                                                       
(processes){                                                                  
Process 93 93 -> 93 93                                                        
Order (*,2)                                                                   
CKKW sqr(17/E_CMS)                                                            
End process;                                                                  
}(processes)                                           
(selector){                                                                    
Rapidity  93 -6.   6. [PT_UP]                                                
PT  93  1120 2240 [PT_UP]                                   
}(selector)     
