ANAPATH=src
ANANAME=InclusiveJets


all: 
	rivet-buildplugin --with-root Rivet${ANANAME}.so ${ANAPATH}/${ANANAME}.cc

## In case one has 'permission denied' problem when using rivet-buildplugin, do
## 'make all-plug-local-setup'
## 'make all-plug-local'
RIVET_BP_PATH=`which rivet-buildplugin`
RIVET_BP_LOCAL=./rivet-buildplugin_local
OLDPATH1=/afs/.cern.ch/sw/
OLDPATH2=/afs/cern.ch/sw/
NEWPATH=${SITEROOT}/sw/
#NEWPATH=/cvmfs/sft.cern.ch/
## see /cvmfs/sft.cern.ch/lcg/
all-plug-local-setup:
	@echo -e "INFO: Creating ${RIVET_BP_LOCAL} from ${RIVET_BP_PATH}"
	cp ${RIVET_BP_PATH} ${RIVET_BP_LOCAL}
	@echo -e "INFO: updating the paths:"
	sed -i "s\${OLDPATH1}\${NEWPATH}\g" ${RIVET_BP_LOCAL}
	sed -i "s\${OLDPATH2}\${NEWPATH}\g" ${RIVET_BP_LOCAL}
	sed -i "s\yoda/1.3.1\yoda/1.3.0\g" ${RIVET_BP_LOCAL}
	#sed -i "s\rivet/2.2.1\rivet/2.2.0\g" ${RIVET_BP_LOCAL}
	chmod 755 rivet-buildplugin_local
all-plug-local:
	./rivet-buildplugin_local --with-root Rivet${ANANAME}.so ${ANAPATH}/${ANANAME}.cc



test:
	Generate_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=123456789 --jobConfig=jobOptions/GenerateJetsJO_PY8_EIG.py --runNumber=100000 --outputEVNTFile=pool.root --maxEvents=100 --env NPC_TUNE=ATLASA14NNPDF NPC_GEN=Pythia8 NPC_EFFECT=PartonLVL



clean:
	rm -rf *.dec *.out *.dat *.DEC G4particle_whitelist.txt *.xml *.xml.BAK *.pickle jobReport.json jobReport.txt ntuple.pmon.gz pdt.table pool.root pool.root*.log runargs.generate.py runwrapper.generate.sh inclusiveP8DsDPlus.pdt log.generate PDGTABLE.MeV TestHepMC.root _joproxy15 Process *.txt

cleanall: clean
	rm *.so *.yoda


