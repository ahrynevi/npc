####################################
### Jet circles at parton/HadOnly/UEOnly/Particle levels.
### Runs on log file produced by match.C.
### Example of the input is logs/log_P8_A14NNPDF_JS9_R04.txt.
###

import math
import re
import matplotlib.pyplot as plt
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--jetR',      help='Example: --jetR 0.4',                   type=float, default=0.4)
parser.add_argument('--tune',      help='Example: --tune A14NNPDF or --tune 4C', type=str,   default="A14NNPDF")
parser.add_argument('--JS',        help='Example: --JS 9',                       type=int,   default=9)
parser.add_argument('--maxEvents', help='Example: --maxEvents 5',                type=int,   default=5)
args=parser.parse_args()

jetR = args.jetR
tune = args.tune
JS   = args.JS
maxEvents=args.maxEvents


#jetR=1.0
#tune="A14NNPDF"
#JS=3

#file1 = open('log_P8_%s_JS%d_R%02.0f.txt' % (tune,JS,jetR*10), 'r')
file1=open('log_fix_10.txt')
Lines = file1.readlines()


nlevels=4
colors=['blue','red','orange','black']
labels=["parton", "parton+had", "parton+UE","particle"]

circleR=math.sqrt(pow(jetR,2)/2)


jets_kin = []
match_info = []
nlines=0
njets=0

iEvent=-1
for line in Lines:
    nlines=nlines+1
    #if nlines==30:
    #    break
    
    info = line.rstrip(' \n')
    info = info.split('|')
    print info

    if "Ev " in info[0]:
        iEvent=iEvent+1
        if iEvent>maxEvents:
            break
        njets=0
        jets_kin.append([])
        match_info.append([])
        for ilevel in range(nlevels):
            jets_kin[iEvent].append([])

        
    if " jet " in info[0] and not "particle level jet" in info[0] and not "parton level jet" in info[0]:
        if "Match jet " in info[0]:
            #print info[0]
            if "Match jet geom within R=0.4: P" in info[0] and not "Strange" in info[0]:
                imatch_info = info[0]
                imatch_info = imatch_info.replace("Match jet geom within R=0.4:","jet")
                print "TO BE ON PLOT: " + imatch_info
                match_info[iEvent].append(imatch_info)
            continue
        njets=njets+1
        for ilevel in range(nlevels):
            jetinfo = info[ilevel+1]
            jetinfo = re.sub(',','',jetinfo)
            jetinfo = re.findall(r'\S+', jetinfo)
            if len(jetinfo)>6:
                jets_kin[iEvent][ilevel].append([jetinfo[2],jetinfo[5],jetinfo[7]])
            #print jetinfo


#print jets_kin


for ievent in range(len(jets_kin)):
    fig, ax = plt.subplots()
    ax.set(xlim=(-5, 5), ylim = (0, 2*math.pi))

    for ilevel in range(nlevels):
        #if ilevel>0:
        #    break
        ijet=0
        for jet in jets_kin[ievent][ilevel]:
            #print ievent, ilevel,  jet, jetR-float(ilevel)/10
            #print jet

            drawR=circleR - circleR/float(nlevels)*float(ilevel)
            #drawR=circleR
            # if-else just for correct legend            
            if ijet==0:
                circle = plt.Circle((jet[1],jet[2]), drawR, color=colors[ilevel],alpha=0.3,label=labels[ilevel])
                ax.add_patch(circle)
            else:
                circle = plt.Circle((jet[1],jet[2]), drawR, color=colors[ilevel],alpha=0.3)
                ax.add_patch(circle)

            ## adding clipped over phi parts
            if float(jet[2]) + circleR > 2*math.pi:
                circle = plt.Circle((float(jet[1]),float(jet[2])-2*math.pi), drawR, color=colors[ilevel],alpha=0.3)
                ax.add_patch(circle)
            if float(jet[2]) - circleR < 0:
                circle = plt.Circle((float(jet[1]),float(jet[2])+2*math.pi), drawR, color=colors[ilevel],alpha=0.3)
                ax.add_patch(circle)

                
            
            dx=0.1
            #dy = circleR - circleR/float(nlevels)*float(ilevel)
            dy = 0.1
            if ilevel==0:
                text="%.0f" % round(float(jet[0]))
                if ijet==0:
                    text="%.0f LEADING" % round(float(jet[0]))
                plt.text(float(jet[1]),   float(jet[2])+dy, text,size=10,color=colors[ilevel], ha='center', va='center')
            if ilevel==3:
                text="%.0f" % round(float(jet[0]))
                if ijet==0:
                    text="%.0f LEADING" % round(float(jet[0]))
                plt.text(float(jet[1]),float(jet[2])-dy,text,size=10,color=colors[ilevel], ha='center', va='center')                
            #ax.add_artist(circle)

            plt.text(-4.8,5.8, "Pythia 8 %s" % tune,  size=15,color=colors[ilevel])                
            plt.text(-4.8,5.4, "Jet R=%1.1f" % jetR,  size=15,color=colors[ilevel])                
            plt.text(-4.8,5.0, "Event %d"    % ievent,size=15,color=colors[ilevel])                
            plt.text(-4.8,4.6, "JS %d"       % JS,    size=15,color=colors[ilevel])

            plt.text(-4.8,0.8, "Match info:", size=7,color='black')
            yshift=0.0
            for iMatch in range(len(match_info[ievent])):
                plt.text(-4.8,0.6-yshift, match_info[ievent][iMatch], size=7,color='black')
                yshift=yshift+0.2

            ijet = ijet + 1
        plt.legend(bbox_to_anchor=(1.1, 1.05))

    saveName='plotcircles_P8_%s_JS%d_R%d_event%d.png' % (tune,JS,int(jetR*10),ievent)
    fig.savefig(saveName)
    print saveName
