#! /usr/bin/env python

import os, sys, array, ROOT
ROOT.gROOT.SetBatch()

input_files = []

if len(sys.argv) > 1:
    input_files = sys.argv[1,-1]
else:
    for f in os.listdir("."):
        if ".yoda" in f: input_files.append( f )
        pass
    pass

for f in input_files:
    convert_command = "yoda2flat " + f
    os.system( convert_command )
    
    in_f = open( f.replace(".yoda",".dat") , "r" )
    
    histos = []
    bins = []
    errors = []
    values = []
    last_bin_upper = 0
    histName = ""
    in_plot = False
    for line in in_f:
        
        if "# END" in line:
            if histName == "": continue
            bins.append( last_bin_upper )
            #print "N bins ", len(bins)
            in_plot = False
            new_histo = ROOT.TH1D( histName, histName, len(bins) - 1, array.array('d', bins ) )
            for bin,v in enumerate(values):
                #print bin, v
                new_histo.SetBinContent( bin+1, v )
                new_histo.SetBinError( bin+1, errors[bin] )
                pass
            histos.append( new_histo )
            bins = []
            values = []
            errors = []
            histName = ""
            last_bin_upper = 0
            continue
        
        elif "# xlow" in line:
            in_plot = True
            continue

        #elif "# BEGIN HISTOGRAM /MC_VBF/" in line:
        #    histName = line.split("# BEGIN HISTOGRAM /MC_VBF/")[1].strip()
        #    continue
        elif "Title=" in line:
            #print line
            histName = line.split('=')[1].strip()
            #print histName
            continue

        elif in_plot:
            bins.append( float(line.split()[0]) )
            values.append( float(line.split()[2]) )
            errors.append( float(line.split()[3]) )
            last_bin_upper = float(line.split()[1])
            pass

        
        pass
    out_file = ROOT.TFile( f.replace(".yoda",".root"), "RECREATE" )
    for h in histos:
        h.Write()
        pass
    out_file.Close()
    in_f.close()
    pass
