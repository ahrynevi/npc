#!/bin/bash

limit=90



echo "start monitoring"
while [ 1 ]; do
    date
    quota=`fs lq | grep hrynev | awk '{sub(/%/,""); print $4}'`
    r_jobs=`qstat -s r | wc -l`
    p_jobs=`qstat -s p | wc -l`
    echo "Current quota filled to "$quota"%"
    echo "Currently running: "${r_jobs}
    echo "Currently pending: "${p_jobs}
    if [ $quota -ge $limit ]; then
	echo "Exceed limit"
	echo "Removing logs"
	rm runBatchArray.sh.o1*
    fi
    sleep 5m
done

