#!/bin/bash

EFFECTS=(Parton Hadr UE UE+Had)

REPEATS_START=0
NREPEATS=9

event=100000

let REPEATS_END=${REPEATS_START}+${NREPEATS}

echo ${REPEATS_START} ${REPEATS_END} ${NREPEATS} 

for REP in `seq ${REPEATS_START} ${REPEATS_END}`; do 

echo ${REP}

    for EFF in ${EFFECTS[@]}; do

    echo ${EFF}

	for J in `seq 1 9`; do
	
	echo ${J}

#	qsub -v REPEAT=$REP,EVENTS=100,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=4C -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=AU2CTEQ -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=AU2CT10 -q inp runBatchArray.sh
# 	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=AU2MSTW -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=AU2NNPDF -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=A2CTEQ -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=A2MSTW -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=AU2MSTW2008LO -q inp runBatchArray.sh
#	qsub -v EVENTS=1000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=MONASH -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14CTEQL1,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14MRSTWO,GEN=Pythia8 -q inp runBatchArray.sh
	qsub -v EVENTS=$event,fEvent=$(($REP * $event+1)),EFFECT=$EFF,JSAMP=$J,TUNE=ATLASA14NNPDF,GEN=Pythia8 -l mem=5gb -q inp runBatchArray.sh 
#	qsub -v EVENTS=$event,fEvent=$(($REP * $event+1)),EFFECT=$EFF,JSAMP=$J,TUNE=NNPDF30NNLO,GEN=Sherpa -l mem=5gb -q inp runBatchArray.sh
#	qsub -v EVENTS=250000,fEvent=$(($REP * $EVENTS+1)),EFFECT=$EFF,JSAMP=$J,TUNE=ATLASA14NNPDFEIG1p,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG1n,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG2p,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG2n,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG3ap,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG3an,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG3bp,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG3bn,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG3cp,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=ATLASA14NNPDFEIG3cn,GEN=Pythia8 -q inp runBatchArray.sh
#	qsub -v EVENTS=100000,fEvent=$(($REP * $EVENTS+1)),EFECT=$EFF,JSAMP=$J,TUNE=NNPDF30NNLO,GEN=Sherpa -l mem=15000mb -q inp runBatchArray.sh
	#qsub -v REPEAT=$REP,EVENTS=100000,RUNNUMBER=$NUMBER,JSAMP=$J,TUNE=LOEE4,GEN=Herwigpp -q inp runBatchArray.sh 
#	qsub -v EVENTS=$event,fEvent=$(($REP * $event+1)),EFFECT=$EFF,JSAMP=$J,TUNE=LOEE4,GEN=Herwigpp -l mem=2gb -q inp runBatchArray.sh
	done
    done
done

