import os
from ROOT import *

mergeReps = True
normJsam  = True
mergeJsam = True
jSamMin=2
jSamMax=4

ANALYSIS="JETS"
runs=[
    "result_ECM13000_ATLASA14NNPDFEIG1p_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG1n_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG2p_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG2n_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3ap_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3an_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3bp_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3bn_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3cp_Run100000_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3cn_Run100000_Events100000",

    "result_ECM13000_ATLASA14NNPDFEIG1p_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG1n_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG2p_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG2n_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3ap_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3an_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3bp_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3bn_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3cp_Run100001_Events100000",
    "result_ECM13000_ATLASA14NNPDFEIG3cn_Run100001_Events100000"
    ]

runtag="v03"

indir  = "../BatchOutput"
outdir = "../Results/v03"

mRepsDir="reps_merged"
mNormDir="jsam_norm"
mJsamDir="jsam_merged"


hists=[
    "jet_pt_AKT04","jet_n_AKT04","jet_eta_AKT04","jet_phi_AKT04",
    "jet_pt_y0_AKT04","jet_pt_y1_AKT04","jet_pt_y2_AKT04","jet_pt_y3_AKT04","jet_pt_y4_AKT04","jet_pt_y5_AKT04","jet_pt_y6_AKT04"
    ]


##########################################
####
####
if mergeReps:
    savedir=outdir+"/"+mRepsDir
    os.system("mkdir -p "+savedir)
    for run in runs:
        print "Merging",run
        for sam in range(jSamMin,jSamMax+1):
            idir="%s/" % (indir)
            listFiles=[]
            for filename in os.listdir(idir):                
                if run+"_JS"+str(sam) in filename and filename.endswith(".root"):
                    #print filename
                    ifile=TFile(idir+filename,"read")
                    if ifile and not ifile.IsZombie() and ifile.Get(ANALYSIS+"/crossSection"):
                        listFiles.append(idir+filename)
                    else:
                        print "Zombie:",idir+filename
            if listFiles:
                command = "hadd -v 0 %s/%s_J%d.root %s" % (savedir,run,sam, ' '.join(listFiles))
                #print command
                os.system( command )
                


##########################################
####
####
if normJsam:
    savedir=outdir+"/"+mNormDir
    os.system("rm -rf "+savedir)
    os.system("mkdir -p "+savedir)
    for run in runs:
        for sam in range(jSamMin,jSamMax+1):
            fileName="%s/%s/%s_J%d.root" %(outdir,mRepsDir,run,sam)
            fj=TFile(fileName,"read")
            xs      = fj.Get(ANALYSIS+"/crossSection").GetBinContent(1)
            nFiles  = fj.Get(ANALYSIS+"/nFiles").GetBinContent(1)
            sumW    = fj.Get(ANALYSIS+"/sumOfWeights").GetBinContent(1)
            nEvents = fj.Get(ANALYSIS+"/nEvents").GetBinContent(1)
            nSelEvents = fj.Get(ANALYSIS+"/nSelectedEvents").GetBinContent(1)
            filtEff = fj.Get(ANALYSIS+"/filtEff").GetBinContent(1)
            sf = xs/nFiles / sumW
            #print nEvents, nSelEvents, sumW

            fileName="%s/%s_J%d.root" % (savedir,run,sam)
            fnew=TFile(fileName,"recreate")
            dd=gDirectory.mkdir(ANALYSIS)
            dd.cd()
            for hist in hists:
                histName  = "%s/%s" % (ANALYSIS,hist)
                htmp=fj.Get( histName)
                #print run,sam,hist
                htmp.Scale(sf)
                htmp.Write(hist)
            fnew.Close()


##########################################
####
####
if mergeJsam:
    savedir=outdir+"/"+mJsamDir
    os.system("rm -rf  "+savedir)
    os.system("mkdir -p "+savedir)
    for run in runs:
        command = "hadd -v 0 %s/%s.root %s/%s/%s_J*.root" %(savedir,run,outdir,mNormDir,run)
        print command
        os.system(command)
