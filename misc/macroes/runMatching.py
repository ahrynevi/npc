import os, sys


jsamples = [1,2,3,4,5,6,7,8,9]

for jsample in jsamples:
    #jsample=2
    partialPath="/Users/alex/Work/NPC/results_J%d_Tune_ATLASA14NNPDF_GEN_Pythia8_EFFECT" % (jsample)
    
    highOrderName="Hadr"
    lowOrderName ="Parton"
    
    fileNames=[
        "result_100000_J%d_TU_ATLASA14NNPDF_fEvent_1_GEN_Pythia8.root" % (jsample),
        "result_100000_J%d_TU_ATLASA14NNPDF_fEvent_100001_GEN_Pythia8.root" % (jsample),
        "result_100000_J%d_TU_ATLASA14NNPDF_fEvent_200001_GEN_Pythia8.root" % (jsample),
        "result_100000_J%d_TU_ATLASA14NNPDF_fEvent_300001_GEN_Pythia8.root" % (jsample),
        "result_100000_J%d_TU_ATLASA14NNPDF_fEvent_400001_GEN_Pythia8.root" % (jsample),
        "result_100000_J%d_TU_ATLASA14NNPDF_fEvent_500001_GEN_Pythia8.root" % (jsample),
        ]
    
    for iFile in range(len(fileNames)):
        command="""root -l -b -q Matching.C\(\\"%s\\",\\"%s\\",\\"%s\\",\\"%s\\"\)""" % (partialPath,highOrderName,lowOrderName,fileNames[iFile])
        print command
        os.system(command)
