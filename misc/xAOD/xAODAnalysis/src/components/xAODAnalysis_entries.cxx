
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../xAODAnalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( xAODAnalysisAlg )

DECLARE_FACTORY_ENTRIES( xAODAnalysis ) 
{
  DECLARE_ALGORITHM( xAODAnalysisAlg );
}
