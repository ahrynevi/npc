
float getGetGenFiltEffFromLog()
{
  cout << "getting GenFiltEff from log" << endl;
  string cfilename="log.generate";
  std::ifstream fileInput;
  fileInput.open(cfilename.c_str());
  char* search = "MetaData: GenFiltEff =";
  unsigned int curLine = 0;
  string line;
  while(getline(fileInput, line)) 
    {
      curLine++;
      if (line.find(search, 0) != string::npos) 
	{
	  stringstream ss(line); 
	  string buf; 
	  vector<string> tokens;
	  while (ss >> buf)
	    tokens.push_back(buf);
	  cout << "found: " << search << "line: " << curLine << endl;
	  cout << "GenFiltEff=" << tokens[tokens.size()-1] <<endl;
	  fileInput.close();
	  return atof(tokens[tokens.size()-1].c_str());
	}
    }
  fileInput.close();
  cout<< "not found :(" <<endl;
  return 1.0;
}

void setFilterEfficiency()
{ 
  TFile *f = new TFile("Rivet.root","update");
  if(f)
    {
      TString histName="filtEff";
      TH1D *h = (TH1D*)f->Get(Form("ZJETS/%s",histName.Data()));
      if(h)
	{
	  TDirectory *d=gDirectory->GetDirectory("ZJETS");
	  d->cd();
	  h->SetBinContent(1,getGetGenFiltEffFromLog() );
	  h->Write(histName,TObject::kOverwrite);
	}
      else
	{
	  cout << "ERROR: No hist in input file" << endl;
	}
      f->Close();
    }
  else
    {
      cout << "ERROR: No input file" << endl;
    }
}



